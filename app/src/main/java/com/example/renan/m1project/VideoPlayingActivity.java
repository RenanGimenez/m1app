package com.example.renan.m1project;

import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.MediaController;
import android.widget.VideoView;

public class VideoPlayingActivity extends AppCompatActivity {
    private String WAMP_SERVER_ADDRESS = "192.168.1.39";
    private String WAMP_SERVER_PORT = "80";
    private String WAMP_SERVER_VIDEOS_PATH = "/videos";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_playing);
        VideoView videoView = findViewById(R.id.videoView2);
        String videoName = (String)getIntent().getSerializableExtra("videoName");
        try {
            videoView.setVideoURI(Uri.parse("http://"+WAMP_SERVER_ADDRESS+":"+WAMP_SERVER_PORT+WAMP_SERVER_VIDEOS_PATH+"/"+videoName));

            MediaController mediaController = new MediaController(this);
            videoView.setMediaController(mediaController);
            mediaController.setAnchorView(videoView);
            videoView.start();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }
}
