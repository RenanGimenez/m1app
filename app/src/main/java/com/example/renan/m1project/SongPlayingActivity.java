package com.example.renan.m1project;

import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class SongPlayingActivity extends AppCompatActivity {
    private String WAMP_SERVER_ADDRESS = "192.168.1.39";
    private String WAMP_SERVER_PORT = "80";
    private String WAMP_SERVER_SONGS_PATH = "/songs";
    static private MediaPlayer mediaPlayer;
    private boolean isStopped = false;
    private String songName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_song_playing);
        if (mediaPlayer != null){
            mediaPlayer.stop();
            mediaPlayer = null;
        }
        songName = (String)getIntent().getSerializableExtra("songName");
        TextView textView = findViewById(R.id.music_name);
        textView.setText(songName);


        initMediaPlayer();
    }

    private void initMediaPlayer() {
        try {
            mediaPlayer = new MediaPlayer();
            // REMOTE

            String url = "http://"+WAMP_SERVER_ADDRESS+":"+WAMP_SERVER_PORT+WAMP_SERVER_SONGS_PATH+"/"+songName;

            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mediaPlayer.setDataSource(url);
            mediaPlayer.prepare();

           /* ContentResolver musicResolver = getContentResolver();
            Uri musicUri = android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;

            Log.e("mediaactivity",musicUri.getPath());
            Log.e("mediaactivity",a);
            if(file.exists())
            {
                Log.e("mediaactivity","true");
            }
            if(!file.exists())
            {
                Log.e("mediaactivity","false");
            }
            mediaPlayer.setDataSource("/external/audio/media/wave.mp3");*/
            //mediaPlayer.setDataSource(this,R.raw.music);
            // mediaPlayer.setDataSource(file.getPath());
            // mediaPlayer.prepare();
            mediaPlayer.start();
      /*  String path ="/sdcard/music.mp3";
        mediaPlayer.setDataSource(path);
        mediaPlayer.prepare();
        mediaPlayer.start();*/
        }
        catch (Exception e)
        {
            e.printStackTrace();

        }
    }

    public void pause(View view) {
        mediaPlayer.pause();
        Log.d("Log","pause");
    }

    public void stop(View view) {
        mediaPlayer.stop();
        isStopped = true;
        Log.d("Log","stop");
    }

    public void play(View view) {
        if(isStopped) {
            initMediaPlayer();
            isStopped = false;
        }
        mediaPlayer.start();
        Log.d("Log","play");
    }

    public void nextSong(View view) {
        mediaPlayer.stop();
        Log.d("Log","nextSong");
        /*songID
        Intent intent = new Intent(this, SongPlayingActivity.class);
        intent.putExtra("songID", songID);
        startActivity(intent);*/
    }


}

