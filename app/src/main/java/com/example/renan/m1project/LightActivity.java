package com.example.renan.m1project;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.SeekBar;

public class LightActivity extends AppCompatActivity{
    SeekBar sb_ld,sb_r,sb_b,sb_g;
    ImageView iv_main;
    Dialog dialog;

    //çº¢è“ç»¿ ä¸‰åŽŸè‰²çš„åˆå§‹å€¼ // red blue green initial value
    private int blue=100;
    private int red=100;
    private int green=100;
    private int alapha=100;

    private int blueProgress=0;
    private int redProgress=0;
    private int greenProgress=0;
    private int alaphaProgress=0;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.read);
        initView();
        permission();
        openAleterWindow();
        initData();

        initListner();

    }

    private void initView() {
        sb_ld=findViewById(R.id.sb_ld);
        sb_r=findViewById(R.id.sb_r);
        sb_b=findViewById(R.id.sb_b);
        sb_g=findViewById(R.id.sb_g);
    }

    private void initListner() {
        sb_ld.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                int add=progress-alaphaProgress;
                alapha=alapha+add;
                alaphaProgress=progress;
                Log.e("this","alapha"+alapha);
                iv_main.setBackgroundColor(Color.argb(alapha, red, green, blue));

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });




        sb_r.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                int add=progress-redProgress;
                red=red-add;
                redProgress=progress;
                Log.e("this","red:"+red);
                iv_main.setBackgroundColor(Color.argb(alapha,red,green,blue));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });


        sb_b.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                int add=progress-blueProgress;
                blue=blue-add;
                blueProgress=progress;
                Log.e("this","blue:"+blue);
                iv_main.setBackgroundColor(Color.argb(alapha,red,green,blue));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        sb_g.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                int add=progress-greenProgress;
                green=green-add;
                greenProgress=progress;
                Log.e("this","green:"+green);
                iv_main.setBackgroundColor(Color.argb(alapha,red,green,blue));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    public  void initData(){    //æ¯å½“æ‰“å¼€acitivy å¯¹ sharedprefernces åˆå§‹åŒ– //once when you activate activity ,instancialize sharedpreferences
        SharedPreferences myPreference=getSharedPreferences("myPreference", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = myPreference.edit();
        editor.putInt("alapha",100);
        editor.putInt("red", 100);
        editor.putInt("blue", 100);
        editor.putInt("green", 100);

        editor.commit();

    }

    public void getData(){  //èŽ·å– å­˜å‚¨ sharePrefrence ä¿å­˜çš„ä¸‰åŽŸè‰²å€¼ // fetch the saved color value in sharedPreferences
        SharedPreferences preferences=getSharedPreferences("myPreference", Context.MODE_PRIVATE);
        alapha=preferences.getInt("alapha",100);
        red=preferences.getInt("red",100);
        green=preferences.getInt("green",100);
        blue=preferences.getInt("blue",100);
        sb_ld.setProgress(alapha-100);
        sb_r.setProgress(100-red);
        sb_g.setProgress(100-green);
        sb_b.setProgress(100-blue);
        iv_main.setBackgroundColor(Color.argb(alapha,red,green,blue));
//        changeAppBrightness(ld);
    }

    public void saveData(){
        SharedPreferences myPreference=getSharedPreferences("myPreference", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = myPreference.edit();
        if(alapha>=0&&alapha<=255&&red>=0&&red<=100&&blue>=0&blue<=100&&green>=0&&green<=100) {
            editor.putInt("alapha", alapha);
            editor.putInt("red", red);
            editor.putInt("blue", blue);
            editor.putInt("green", green);
            editor.commit();
        }


    }


    private void openAleterWindow() {   //æ‰“å¼€ dailog çª—å£ å¯¹ dailog åˆå§‹åŒ– //open dailog windows,and initialize dailog

        dialog=new Dialog(this,R.style.dialog_translucent);
        dialog.setContentView(R.layout.dailog);


        WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.flags =WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL |
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE;  //è®¾ç½®ä¸å½±å“ä¸‹å±‚çš„è§¦ç¢° //make sure not to affect uperclass action
//
//        lp.type = WindowManager.LayoutParams.TYPE_SYSTEM_ALERT
//                | WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY;   //è®¾ç½®é¡¶å±‚
        if(Build.VERSION.SDK_INT>=26){
            lp.type=WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY;
        }else{
            lp.type=WindowManager.LayoutParams.TYPE_SYSTEM_OVERLAY;
        }

        dialog.getWindow().setAttributes(lp);

        dialog.show();



        iv_main=dialog.findViewById(R.id.ll_main);
        getData();


    }



    public void permission(){
        if (Build.VERSION.SDK_INT >= 23) {
            if(!Settings.canDrawOverlays(this)) {
                Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION);
                startActivity(intent);
            }
        }
    }


    public void changeAppBrightness(int brightness) {   //æ”¹å˜ç³»ç»Ÿå±å¹•äº®åº¦ //change the brightness of screen
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        Float ld=Float.valueOf(brightness) * (1f / 100f);
        Log.e("this","ld:"+ld);
        lp.screenBrightness = ld;

        getWindow().setAttributes(lp);
    }


}

