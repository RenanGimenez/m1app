package com.example.renan.m1project;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private int MY_PERMISSIONS_REQUEST_OK = 0;
    Button bt_lightbutton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
        initListener();
        checkPermissions();


    }

    private void initListener() {

        bt_lightbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, LightActivity.class);
                startActivity(intent);
            }
        });
    }

    private void initView() {
        bt_lightbutton=findViewById(R.id.bt_lightbutton);
    }

    /* MusicButton onClick */
    public void startMusicList(View view) {

        Intent intent = new Intent(this, MusicListActivity.class);
        startActivity(intent);
    }


    private void checkPermissions() {
        if ((ContextCompat.checkSelfPermission(this, Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED) ||
                (ContextCompat.checkSelfPermission(this, Manifest.permission.WAKE_LOCK) != PackageManager.PERMISSION_GRANTED))

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WAKE_LOCK,Manifest.permission.SYSTEM_ALERT_WINDOW},
                    1);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
        } else {
            Toast.makeText(this, "You must give all pemissions asked before using the app", Toast.LENGTH_LONG).show();
        }

    }

    public void startVideoList(View view) {
        Intent intent = new Intent(this, VideoListActivity.class);
        startActivity(intent);
    }
}
