package com.example.renan.m1project;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MediaView extends LinearLayout {

    private  ImageView imageView;
    private  TextView textView;
    public MediaView(Context context, AttributeSet attributeSet) {
        super(context);
        inflate(context, R.layout.media_view, this);
        imageView = findViewById(R.id.media_image);
        textView = findViewById(R.id.media_info);
        TypedArray attributes = context.obtainStyledAttributes(attributeSet, R.styleable.MediaView);
        imageView.setImageDrawable(attributes.getDrawable(R.styleable.MediaView_image));
        textView.setText(attributes.getString(R.styleable.MediaView_text));
    }

    public void setImageView(int value){
        imageView.setBackgroundResource(value);
    }
    public void setText(String mediaName) {
        textView.setText(mediaName);
    }
    public String getText(){return textView.getText().toString();}
}
