package com.example.renan.m1project;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class MusicListActivity extends AppCompatActivity {
    private String JAVA_SERVER_ADDRESS = "192.168.1.39";
    private String JAVA_SERVER_PORT = "8080";
    private String JAVA_SERVER_GET_SONGS_PATH = "/java/songs";
    private String JSonResult;
    private Object LOCK;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_music_list);
        JSonResult = null;
        LOCK = new Object();
        Log.d("Order", "1");

        JSonTask jSonTask = new JSonTask(JSonResult, LOCK);
        jSonTask.execute("http://"+JAVA_SERVER_ADDRESS+":"+JAVA_SERVER_PORT+JAVA_SERVER_GET_SONGS_PATH);
        Log.d("Order", "?");

        synchronized (LOCK) {
            while ((JSonResult = jSonTask.getJSonResult()) == null) {
                try {
                    LOCK.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        Log.d("FinalResult", JSonResult);
        try {
            addMediaViews(new JSONArray(JSonResult));

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void addMediaViews(JSONArray jSonRoot) {
        try {
            int columnCount = 3;
            LinearLayout rootLayout = findViewById(R.id.linearLayoutRoot);
            LinearLayout horizontalLayout = null;
            for (int i = 0; i < jSonRoot.length(); ++i){
                if(i%columnCount == 0){
                    /* Create horizontal layout inside the vertical layout.
                    <LinearLayout android:layout_width="match_parent"
                                    android:layout_height="match_parent"
                                    android:layout_weight="1"
                                    android:orientation="horizontal"/>
                     */
                    horizontalLayout = new LinearLayout(this);
                    horizontalLayout.setOrientation(LinearLayout.HORIZONTAL);
                    horizontalLayout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT,1));
                    rootLayout.addView(horizontalLayout);
                }
                JSONObject jSonMedia = jSonRoot.getJSONObject(i);
                String mediaName = (String) jSonMedia.get("name");
                Log.d("MediaName", mediaName);
                /* Create media view inside the horizontal layout. */
                MediaView mediaView = new MediaView(this, null);
                mediaView.setText(mediaName);
                mediaView.setImageView(R.drawable.music_5);
                mediaView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT,1));
                mediaView.setPadding(0,0,0,0);
                mediaView.setOnClickListener(myOnClick);
                horizontalLayout.addView(mediaView);

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    LinearLayout.OnClickListener myOnClick = new LinearLayout.OnClickListener() {

        public void onClick(View v) {
            MediaView mediaView = (MediaView) v;
            Intent intent = new Intent(MusicListActivity.this, SongPlayingActivity.class);
            intent.putExtra("songName",mediaView.getText());
            startActivity(intent);
        }
    };


    private class JSonTask extends AsyncTask<String, String, String> {
        private String JSonResult;
        private Object LOCK;

        public JSonTask(String JSonResult, Object LOCK){
            this.JSonResult = JSonResult;
            this.LOCK = LOCK;
        }
        protected void onPreExecute() {
            super.onPreExecute();
            Log.d("Order", "2");
        }

        protected String doInBackground(String... params) {

            Log.d("Order", "3");
            HttpURLConnection connection = null;
            BufferedReader reader = null;
            StringBuffer result;
            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();


                InputStream stream = connection.getInputStream();

                reader = new BufferedReader(new InputStreamReader(stream));

                result = new StringBuffer();
                String line;

                while ((line = reader.readLine()) != null) {
                    result.append(line + "\n");
                    Log.d("Response: ", "> " + line);   //here u ll get whole response...... :-)
                }


            } catch (IOException e) {
                e.printStackTrace();
                result = new StringBuffer().append("Error");
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            Log.d("Order", "4");
            synchronized (LOCK) {
                JSonResult = result.toString();
                LOCK.notifyAll();
            }
            return JSonResult;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Log.d("Order", "5");

        }

        public String getJSonResult() {
            return JSonResult;
        }
    }

}
